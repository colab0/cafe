import React from 'react';
import "materialize-css/dist/css/materialize.min.css";
import "material-design-icons/iconfont/material-icons.css";
import "./App.css";
import "jquery/dist/jquery.min.js";
import "materialize-css/dist/js/materialize.min.js";
// Import Materialize
import M from "materialize-css";

import {BrowserRouter as Router, Route, Switch} from "react-router-dom";

import Navbar from "./components/navbar.component";
import EditUser from "./components/edit-user.component";
import CreateUser from "./components/create-user.component";
import UsersList from "./components/UsersList.component";

import CreateCategory from './components/create-category.component';
import CategoriesList from './components/CategoriesList.component';
import EditCategory from "./components/edit-category.component";
import CreateItem from "./components/create-item.component";
import ItemsList from "./components/ItemsList.component";
import EditItem from "./components/edit-item.component";
import StoreView from "./components/store.component";



function App() {
  return (
    <div className="row">
      <Router>
      <Navbar/>
      <br/>
  
  <switch>
      <Route path="/category/edit/:id" exact   component={EditCategory}/>
      <Route path="/category" exact component={CreateCategory}/>
      <Route path="/edit/:id" component={EditUser} />
      <Route path="/user" component={CreateUser} />
      <Route path="/categories" component={CategoriesList}/>


      <Route path="/items/edit/:id" exact component = {EditItem}/>
      <Route path="/item" exact component = {CreateItem}/>
      <Route path="/items" exact component = {ItemsList}/>
      <Route path="/store" exact component = {StoreView}/>
    


      <Route path="/" exact component = {UsersList}/>

      
  </switch>
      
    </Router>
    </div>
  );
}

export default App;
