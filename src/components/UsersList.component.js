import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const User = props => (
  <tr>
    <td>{props.user.username}</td>
    <td>{props.user.lastname}</td>
    <td>{props.user.email}</td>
    <td>{props.user.phone}</td>
    <td>
      <Link to={"/edit/"+props.user._id} className="btn-floating teal lighten-1 left-align"><i className="material-icons white-text ">mode_edit</i></Link> <a href="#" onClick={() => { props.deleteUser(props.user._id) }} className="btn-floating red lighten-1 right" ><i className="material-icons white-text">delete</i></a>
    </td>
  </tr>
)

export default class UsersList extends Component {
  constructor(props) {
    super(props);
    this.deleteUser = this.deleteUser.bind(this)
    this.state = {users: []};
  }

  componentDidMount() {
    axios.get('http://localhost:5000/users/')
      .then(response => {
        this.setState({ users: response.data })
      })
      .catch((error) => {
        console.log(error);
      })
  }

  deleteUser(id) {
    axios.delete('http://localhost:5000/users/'+id)
      .then(response => { console.log(response.data)});

    this.setState({
      users: this.state.users.filter(el => el._id !== id)
    })
  }

  userList() {
    return this.state.users.map(currentuser => {
      return <User user={currentuser} deleteUser={this.deleteUser} key={currentuser._id}/>;
    })
  }

  render() {
    return (
        <div className="container" >
            <div>
                <p className="">
                <h4 className="chip secondary-text"> <i className="material-icons md-36 left">face</i> Registered Users</h4>
                </p>
        <table className="table centered striped responsive-table">
          <thead className="thead-light">
            <tr className="main-text">
              <th>Name</th>
              <th>Lastname</th>
              <th>Email</th>
              <th>Phone</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            { this.userList() }
          </tbody>
        </table>
      </div>
        </div>
    )
  }
}