import React, {Component} from "react";
import axios from "axios";

export default class EditCategory extends Component{
    constructor(props){
        super(props);
    
        this.onChangeDescription = this.onChangeDescription.bind(this);
        this.onChangeIcon = this.onChangeIcon.bind(this);
        this.onSubmit         = this.onSubmit.bind(this);

        this.state = {
            description: '',
            icon: ''    
        }   
    }

    componentDidMount() {
        axios.get('http://localhost:5000/categories/'+this.props.match.params.id)
        .then(response => {
            this.setState({
              description: response.data.description,
              icon: response.data.icon
            })
        })
        .catch(function (error) {
            console.log(error);
        })
       
        axios.get('http://localhost:5000/categories/')
          .then(response => {
            if (response.data.length > 0) {  
              this.setState({
                categories: response.data.map(category => category.description),
              })
            }
          })
          .catch((error) => {
            console.log(error);
          })
      
      }

    onChangeDescription(e){
        this.setState({
          description : e.target.value
        });
      }
      
      onChangeIcon(e){
        this.setState({
          icon : e.target.value
        });
      }
      onSubmit(e) {
          e.preventDefault();
      
          const category = {
              description: this.state.description,
              icon: this.state.icon
          };

          console.log(category);

              axios.post('http://localhost:5000/categories/update/'+this.props.match.params.id,category)
              .then(res => console.log(res.data));
          
              this.setState({
                description: '',
                icon: ''
                  });
          
          window.location = '/categories';
      }

      render(){
          return(
              <div className="container">
                  <div className="row">
                     <div className="col m6 s12 offset-m3">
                         <div className="card-image">
                             <div className="card-title hide">Edit Category</div>
                             <form className="card-content" onSubmit={this.onSubmit}>
                             <h4 className="secondary-text chip">Edit Category</h4>
                            <br/>
                                <div className="input-field col s12">
                                    <i className="material-icons prefix">mode_edit</i>
                                    <input placeholder="" id="description" type="text" className="validate" value={this.state.description} onChange={this.onChangeDescription} />
                                    <label for="description">Description bla</label>
                                </div>

                                <div className="input-field col s12">
                                    <i className="material-icons prefix">insert_emoticon</i>
                                    <input placeholder="" id="icon" type="text" className="validate" value={this.state.icon} onChange={this.onChangeIcon} />
                                    <label for="icon">icon</label>
                                </div>

                                <button className="btn waves-effect waves-light right" type="submit" name="action"> <strong>Save</strong>
                                    <i className="material-icons right">save</i>
                                </button>
                             </form>
                         </div>
                     </div>
                  </div>
              </div>
          )
      }
}