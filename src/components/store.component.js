import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

// Import Materialize
import M, { Tabs } from "materialize-css";

const Item = props => (
  <tr>
    <td>{props.item.title}</td>
    <td>{props.item.description}</td>
    <td>{props.item.quantity}</td>
    <td>{props.item.amount}</td>
    <td>{props.item.images}</td>
    <td className="center">
      <Link to={"items/edit/"+props.item._id} className="btn-floating teal lighten-1 left-align"><i className="material-icons white-text ">mode_edit</i></Link> <a href="#" onClick={() => { props.deleteItem(props.item._id) }} className="btn-floating red lighten-1 right" ><i className="material-icons white-text">delete</i></a>
    </td>
  </tr>
)

const CategoryTab = props => (
    <li className="tab col m2 s3 center"><a to="#test1"  href={'#'+props.category.description} className=""> <i className="material-icons">local_cafe</i> {props.category.description}</a></li>
)

const CategoryDisplay = props => (
  <div id={props.category.description} className={'col s12 '+props.classn}>{props.category.description}</div>
)

export default class StoreView extends Component {
  constructor(props) {
    super(props);
    
    this.deleteItem = this.deleteItem.bind(this)

    this.state = {
      count: '0',
      items: [],
      categories: []
    }
  }

  componentDidMount() {
    axios.get('http://localhost:5000/items/all')
      .then(response => {
        this.setState({ items: response.data })
      })
      .catch((error) => {
        console.log(error);
      })

      axios.get('http://localhost:5000/categories/all')
      .then(response => {
        this.setState({ categories: response.data })
      })
      .catch((error) => {
        console.log(error);
      })

          document.addEventListener('DOMContentLoaded', function() {
            M.AutoInit();
            var elem = document.querySelector('#categoriesTab');
            var instance =  M.Tabs.init(elem);


          });

  
      // instance.select('todo');
  }

  // componentWillMount(){
  //   document.addEventListener('DOMContentLoaded', function() {
  //     M.AutoInit();
  //     var elem = document.querySelectorAll('#categoriesTab');
  //     var instance =  M.Tabs.init(elem,{swipeable: true,});

  //   });
  // }

  deleteItem(id) {
    axios.delete('http://localhost:5000/items/'+id)
      .then(response => { console.log(response.data)});
    this.setState({
      items: this.state.items.filter(el => el._id !== id)
    })
  }

  itemList() {
    return this.state.items.map(currentitem => {
      return <Item item={currentitem} deleteItem={this.deleteItem} key={currentitem._id}/>;
    })
  }

  categoryTabList() {
    return this.state.categories.map(currentcategory => {
      return <CategoryTab category={currentcategory} key={currentcategory._id}/> ;
    })
  }

categoryDisplayList(){
  return this.state.categories.map(currentcategorydisplay => {
    if(this.state.count  === '0'){
      this.setState({ count:7});
      return <CategoryDisplay category={currentcategorydisplay} key={currentcategorydisplay}  classn={"actives"} />;
    }     
    return <CategoryDisplay category={currentcategorydisplay} key={currentcategorydisplay} classn={"s"}/>;
  })
}

Tabss() {
  return <div className="row">
  <div className="col s12">
    <ul className="tabs" id="categoriesTab">
      {this.categoryTabList()}
    <li className="tab col hide" id=""><a href="#todo" className="active">Todo</a></li>               
    </ul>
  </div>
{this.categoryDisplayList()}
</div>;
}

  render() {
    return ( 
    <div className="container" >
            <div>
                <p className="">
                <h4 className="chip secondary-text"> <i className="material-icons md-36 left">store</i> Explore, Enjoy at the first sight! ☕️ 😃</h4>
                </p>
        <table className="table  striped responsive-table hide">
          <thead className="thead-light">
            <tr className="main-text">
              <th><i className="material-icons left">store</i>Producto</th>
              <th><i className="material-icons left">description</i>Descripción</th>
              <th><i className="material-icons left">label</i>Cantidad</th>
              <th><i className="material-icons left">attach_money</i>Precio</th>
              <th> <i className="material-icons left">insert_photo</i> Imágenes</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            { this.itemList() }
          </tbody>
        </table>
        {this.Tabss()}
      </div>
        </div>
    )
  }
}