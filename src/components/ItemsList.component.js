import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const Item = props => (
  <tr>
    <td>{props.item.title}</td>
    <td>{props.item.description}</td>
    <td>{props.item.quantity}</td>
    <td>{props.item.amount}</td>
    <td>{props.item.images}</td>
    <td className="center">
      <Link to={"items/edit/"+props.item._id} className="btn-floating teal lighten-1 left-align"><i className="material-icons white-text ">mode_edit</i></Link> <a href="#" onClick={() => { props.deleteItem(props.item._id) }} className="btn-floating red lighten-1 right" ><i className="material-icons white-text">delete</i></a>
    </td>
  </tr>
)

export default class ItemsList extends Component {
  constructor(props) {
    super(props);
    this.deleteItem = this.deleteItem.bind(this)
    this.state = {items: []};
  }

  componentDidMount() {
    axios.get('http://localhost:5000/items/all')
      .then(response => {
        this.setState({ items: response.data })
      })
      .catch((error) => {
        console.log(error);
      })
  }

  deleteItem(id) {
    axios.delete('http://localhost:5000/items/'+id)
      .then(response => { console.log(response.data)});

    this.setState({
      items: this.state.items.filter(el => el._id !== id)
    })
  }

  itemList() {
    return this.state.items.map(currentitem => {
      return <Item item={currentitem} deleteItem={this.deleteItem} key={currentitem._id}/>;
    })
  }

  render() {
    return (
        <div className="container" >
            <div>
                <p className="">
                <h4 className="chip secondary-text"> <i className="material-icons md-36 left">local_cafe</i> Added Items</h4>
                </p>
        <table className="table  striped responsive-table">
          <thead className="thead-light">
            <tr className="main-text">
              <th><i className="material-icons left">store</i>Producto</th>
              <th><i className="material-icons left">description</i>Descripción</th>
              <th><i className="material-icons left">label</i>Cantidad</th>
              <th><i className="material-icons left">attach_money</i>Precio</th>
              <th> <i className="material-icons left">insert_photo</i> Imágenes</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            { this.itemList() }
          </tbody>
        </table>
      </div>
        </div>
    )
  }
}