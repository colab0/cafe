import React, {Component} from "react";
import { Link } from "react-router-dom";


export default class Navbar extends Component{
    render(){
        return(
            <nav className="brown darken-4">
    <div className="nav-wrapper">
      <Link to="#" className="brand-logo center hide"><i className="material-icons md-36 white-text">local_cafe</i>KAPE</Link>
      <ul id="nav-mobile" className="left hide-on-med-and-down">
        <li><Link to="/">Users</Link></li>
        <li><Link to="/user">Create User</Link></li>
        <li><Link to="/edit/:id">Edit User</Link></li>

        <li><Link to="/categories">Categories</Link></li>
        <li><Link to="/category">CreateCategory</Link></li>
        <li><Link to="/category/edit/:id">Edit Category</Link></li>

        <li><Link to="/item">Create Item</Link></li>
        <li><Link to="/items">Items</Link></li>
        <li><Link to="/store"><i className="material-icons white-text">store</i></Link></li>


      </ul>
    </div>
  </nav>

        )
    }

}