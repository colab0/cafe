import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const Category = props => (
  <tr>
    <td>{props.category.description}</td>
    <td>{props.category.icon}</td>
    <td>
      <Link to={"/category/edit/"+props.category._id} className="btn-floating teal lighten-1 left-align"><i className="material-icons white-text ">mode_edit</i></Link> <a href="#" onClick={() => { props.deleteCategory(props.category._id) }} className="btn-floating red lighten-1 right" ><i className="material-icons white-text">delete</i></a>
    </td>
  </tr>
)

export default class CategoriesList extends Component {
  constructor(props) {
    super(props);
    this.deleteCategory = this.deleteCategory.bind(this)
    this.state = {categories: []};
  }

  componentDidMount() {
    axios.get('http://localhost:5000/categories/')
      .then(response => {
        this.setState({ categories: response.data })
      })
      .catch((error) => {
        console.log(error);
      })
  }

  deleteCategory(id) {
    axios.delete('http://localhost:5000/categories/'+id)
      .then(response => { console.log(response.data)});

    this.setState({
      categories: this.state.categories.filter(el => el._id !== id)
    })
  }

  categoryList() {
    return this.state.categories.map(currentcategory => {
      return <Category category={currentcategory} deleteCategory={this.deleteCategory} key={currentcategory._id}/>;
    })
  }

  render() {
    return (
        <div className="container" >
            <div>
                <p className="">
                <h4 className="chip secondary-text"> <i className="material-icons md-36 left">face</i> Categories Created</h4>
                </p>
        <table className="table centered striped responsive-table">
          <thead className="thead-light">
            <tr className="main-text">
              <th>Description</th>
              <th>Icon</th>
            </tr>
          </thead>
          <tbody>
            { this.categoryList() }
          </tbody>
        </table>
      </div>
        </div>
    )
  }
}