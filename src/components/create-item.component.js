import React, {Component} from "react";
import axios from "axios";
// Import Materialize
import M from "materialize-css";

export default class CreateItem  extends Component{
constructor(props){
    super(props);

    this.onChangeTitle    = this.onChangeTitle.bind(this);
    this.onChangeDescription    = this.onChangeDescription.bind(this);
    this.onChangeQuantity    = this.onChangeQuantity.bind(this);
    this.onChangeAmount    = this.onChangeAmount.bind(this);
    this.onChangeImages    = this.onChangeImages.bind(this);
    this.onChangeCategory    = this.onChangeCategory.bind(this);
    this.onSubmit         = this.onSubmit.bind(this);

    this.state = {
        title : '',
        description : '',
        quantity : '',
        amount : '',
        images : '',
        category : ''
    }   
}

componentDidMount() {

    M.AutoInit();
  
    axios.get('http://localhost:5000/categories')
      .then(response => {
        if (response.data.length > 0) {
          this.setState({
            categories: response.data.map(category => category.username),
            // username: response.data[0].username
          })
        }
      })
      .catch((error) => {
        console.log(error);
      })
  }


onChangeTitle(e){
    this.setState({
        title : e.target.value
    });
}
onChangeDescription(e){
    this.setState({
        description : e.target.value
    });
}
onChangeQuantity(e){
    this.setState({
        quantity : e.target.value
    });
}
onChangeAmount(e){
    this.setState({
        amount : e.target.value
    });
}
onChangeImages(e){
    this.setState({
        images : e.target.value
    });
}
onChangeCategory(e){
    this.setState({
        category : e.target.value
    });
}

onSubmit(e) {
    e.preventDefault();

    const item = {
        title: this.state.title,
        description: this.state.description,
        quantity: this.state.quantity,
        amount: this.state.amount,
        images: this.state.images,
        category: this.state.category,
    };

    axios.post('http://localhost:5000/items/add',item)
    .then(res => console.log(res.data));

    this.setState({
        title : '',
        description : '',
        quantity : '',
        amount : '',
        images : '',
        category : ''
        });

    window.location = '/items';
}

render(){
    return (
        // <h1>Hello</h1>
        <div className="container">
            <div className="row">
            <div className="col m6 s12 offset-m3">
                         <div className="card-image">
                             <div className="card-title hide">Add Product</div>
                             <form className="card-content" onSubmit={this.onSubmit}>
                             <h4 className="secondary-text chip">Add Product</h4>
                                <br/>
                                <div className="input-field col s12">
                                    <i className="material-icons prefix">store</i>
                                    <input placeholder="" id="title" type="text" className="validate" value={this.state.title} onChange={this.onChangeTitle} />
                                    <label for="title">Producto</label>
                                </div>

                                <div className="input-field col s12">
                                    <i className="material-icons prefix">description</i>
                                    <input placeholder="" id="description" type="text" className="validate" value={this.state.description} onChange={this.onChangeDescription} />
                                    <label for="description">description</label>
                                </div>

                                <div className="input-field col s12">
                                    <i className="material-icons prefix">label</i>
                                    <input placeholder="" id="quantity" type="text" className="validate" value={this.state.quantity} onChange={this.onChangeQuantity} />
                                    <label for="quantity">Cantidad</label>
                                </div>

                                <div className="input-field col s12">
                                    <i className="material-icons prefix">attach_money</i>
                                    <input placeholder="" id="amount" type="text" className="validate" value={this.state.amount} onChange={this.onChangeAmount} />
                                    <label for="amount">Precio</label>
                                </div>

                                <div className="input-field col s12">
                                    <i className="material-icons prefix">insert_photo</i>
                                    <input placeholder="" id="image" type="text" className="validate" value={this.state.images} onChange={this.onChangeImages} />
                                    <label for="image">Imagen</label>
                                </div>


                                <button className="btn waves-effect waves-light right" type="submit" name="action"> <strong>Create</strong>
                                    <i className="material-icons right">add</i>
                                </button>
                             </form>
                         </div>
                     </div>
            </div>
        </div>
        )
    }
}