import React, {Component} from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import axios from "axios";

export default class EditUser  extends Component{
constructor(props){
    super(props);

    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.onChangeLastname = this.onChangeLastname.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
    this.onChangeEmail    = this.onChangeEmail.bind(this);
    this.onChangePhone    = this.onChangePhone.bind(this);
    this.onSubmit         = this.onSubmit.bind(this);

    this.state = {
        username: '',
        lastname: '',
        password: '' ,
        email: '',
        phone: '',
        users: []
    }   
}

componentDidMount() {
  axios.get('http://localhost:5000/users/'+this.props.match.params.id)
  .then(response => {
      this.setState({
        username: response.data.username,
        lastname: response.data.lastname,
        password: response.data.password,
        phone: response.data.phone,
        email: response.data.email
      })
  })
  .catch(function (error) {
      console.log(error);
  })
 
  axios.get('http://localhost:5000/users/')
    .then(response => {
      if (response.data.length > 0) {
        this.setState({
          users: response.data.map(user => user.username),
        })
      }
    })
    .catch((error) => {
      console.log(error);
    })

}

onChangeUsername(e){
    this.setState({
        username : e.target.value
    });
}

onChangeLastname(e){
  this.setState({
    lastname : e.target.value
  });
}

onChangePassword(e){
  this.setState({
    password : e.target.value
  });
}

onChangeEmail(e){
  this.setState({
    email : e.target.value
  });
}

onChangePhone(e){
  this.setState({
    phone : e.target.value
  });
}
onSubmit(e) {
    e.preventDefault();

    const user = {
        username: this.state.username,
        lastname: this.state.lastname,
        password: this.state.password,
        phone: this.state.phone,
        email: this.state.email
    };

    console.log(user);

    axios.post('http://localhost:5000/users/update/'+this.props.match.params.id,user)
    .then(res => console.log(res.data));

    this.setState({
      username: '',
      lastname: '',
      password: '',
      phone: '',
      email:''
        });

    window.location = '/user';
}
    render(){
        return(
            <div className="container">
                <div className="row">
                    <div className="col s6 offset-s3 card">
                    <div className="card-image waves-effect waves-block waves-light">
                    <div className="header-background"></div>
    </div>
    <form className="card-content col s12" onSubmit={this.onSubmit}>
    <h4 className="secondary-text chip">Edit User</h4>
    <br/>

    <div className="input-field col s12">
    <select ref="userInput" required value={this.state.username} onChange={this.OnChangeUsername}>
    {
          this.state.users.map(function(user){
            return <option key={user}
            value={user}> {user}
            </option>
          })
          }
    </select>
    <label>Materialize Select</label>
  </div>

      <div className="row">

        <div className="input-field col s12">
        <i className="material-icons prefix">account_circle</i>
          <input placeholder="" id="first_name" type="text" className="validate" value={this.state.username} onChange={this.onChangeUsername} />
          <label for="first_name">First Name</label>
        </div>

        <div className="input-field col s12">
        <i className="material-icons prefix">groups</i>
          <input id="last_name" type="text" className="validate" value={this.state.lastname} onChange={this.onChangeLastname}/>
          <label for="last_name">Last Name</label>
        </div>
      </div>

      <div className="row">
        <div className="input-field col s12">
        <i className="material-icons prefix">call</i>
          <input id="phone" type="tel" className="validate" value={this.state.phone} onChange={this.onChangePhone} />
          <label for="phone">Phone</label>
        </div>
      </div>
      
      <div className="row">
        <div className="input-field col s12">
        <i className="material-icons prefix">email</i>
          <input id="email" type="email" className="validate" value={this.state.email} onChange={this.onChangeEmail} />
          <label for="email">Email</label>
        </div>
      </div>

      <div className="row">
        <div className="input-field col s12">
        <i className="material-icons prefix">vpn_key</i>
          <input id="password" type="password" className="validate" value={this.state.password} onChange={this.onChangePassword}/>
          <label for="password">Password</label>
        </div>
      </div>

      <button className="btn waves-effect waves-light right" type="submit" name="action"> <strong>Save</strong>
    <i className="material-icons right">send</i>
  </button>

    </form>
  </div>
                </div>    
            </div>
        )
    }
}