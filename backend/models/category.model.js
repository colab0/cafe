
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const CategorySchema = new Schema({
    description:{
        type: String,
        require: true,
        unique: true,
        trim: true,
        maxlength:10
    },
    icon:{
        type: String
    }
},
{
   timestamps:true, 
});

const Category = mongoose.model('Category', CategorySchema);

module.exports = Category;