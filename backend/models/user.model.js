
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
    username:{
        type: String,
        require: true,
        unique: true,
        trim: true,
        minlength:3
    },
    lastname:{
        type:String,
        require:true,
        trim:true,
        maxlength:15
    },
    email:{
        type: String
    },
    phone:{
        type:String,
        maxlength:8
    },
    password:{
        type: String
    }
},
{
   timestamps:true, 
});

const User = mongoose.model('User', UserSchema);

module.exports = User;