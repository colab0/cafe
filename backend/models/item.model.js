
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ItemSchema = new Schema({
    title:{
        type: String,
        require: true,
        maxlength:25
    },
    description:{
        type: String
    },
    quantity:{
        type: Number,
        require: true
    },
    amount:{
        type: Number,
        require:true
    },
    images:[{
        type:String
    }],
    category:{
        type: String

    }
},
{
   timestamps:true, 
});

const Item = mongoose.model('Item', ItemSchema);

module.exports = Item;