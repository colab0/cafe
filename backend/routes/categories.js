const router = require('express').Router();
let Category = require('../models/category.model');

// Obtener categorias 
router.route('/all').get((req, res) => {
    Category.find()
    .then(categories => res.json(categories))
    .catch(err => res.status(400).json('Error: ' + err));
});

// Agregar Categoria
router.route('/add').post((req, res) => {
 const description = req.body.description;
 const icon = req.body.icon;

 const newCategory = new Category({
     description,
     icon
 });

 newCategory.save()
 .then(() => res.json('Category added!'))
 .catch(err => res.status(400).json('Error: ' + err));

});

// Obtener Categoria
router.route('/:id').get((req,res) => {
    Category.findById(req.params.id)
    .then(category => res.json(category))
    .catch(err => res.status(400).json('Error: '+err));
});

// Eliminar usuario
router.route('/:id').delete((req,res) => {
    Category.findByIdAndDelete(req.params.id)
    .then(category => res.json('Categoria Eliminado'))
    .catch(err => res.status(400).json('Error: '+err));
});

// Actualizar usuario
router.route('/update/:id').post((req,res) => {
    Category.findById(req.params.id)
    .then( category => {
        category.description = req.body.description;
        category.icon = req.body.icon;
        category.save()
        .then(() => res.json('Categoria Acutalizado'))
    .catch(err => res.status(400).json('Error: '+err));

    })
    .catch(err => res.status(400).json('Error: '+err));
});


module.exports = router;