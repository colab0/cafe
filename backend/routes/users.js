const router = require('express').Router();
let User = require('../models/user.model');

// Obtener usuarios
router.route('/').get((req, res) => {
    User.find()
    .then(users => res.json(users))
    .catch(err => res.status(400).json('Error: ' + err));
});

// Agregar usuario
router.route('/add').post((req, res) => {
 const username = req.body.username;
 const lastname = req.body.lastname;
 const email =  req.body.email;
 const phone = req.body.phone;
 const password = req.body.password;

 const newUser = new User({
     username,
     lastname,
     email,
     phone,
     password
 });

 newUser.save()
 .then(() => res.json('User added!'))
 .catch(err => res.status(400).json('Error: ' + err));

});

// Mostrar Usuario
router.route('/:id').get((req,res) => {
    User.findById(req.params.id)
    .then(user => res.json(user))
    .catch(err => res.status(400).json('Error: '+err));
});

// Eliminar usuario
router.route('/:id').delete((req,res) => {
    User.findByIdAndDelete(req.params.id)
    .then(user => res.json('Usuario Eliminado'))
    .catch(err => res.status(400).json('Error: '+err));
});

// Actualizar usuario
router.route('/update/:id').post((req,res) => {
    User.findById(req.params.id)
    .then( user => {
        user.username = req.body.username;
        user.lastname = req.body.lastname;
        user.email =  req.body.email;
        user.phone = req.body.phone;
        user.password = req.body.password;
        user.save()
        .then(() => res.json('Usuario Acutalizado'))
    .catch(err => res.status(400).json('Error: '+err));

    })
    .catch(err => res.status(400).json('Error: '+err));
});

module.exports = router;