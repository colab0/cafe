const router = require('express').Router();
let Item = require('../models/item.model');

// Obtener articulos
router.route('/all').get((req, res) => {
    Item.find()
    .then(items => res.json(items))
    .catch(err => res.status(400).json('Error: ' + err));
});


// Agregar articulo
router.route('/add').post((req, res) => {
    const title = req.body.title;
    const description = req.body.description;
    const quantity =  req.body.quantity;
    const amount = req.body.amount;
    const images = req.body.images;
    const category = req.body.category;
   
    const newItem = new Item({
        title,
        description,
        quantity,
        amount,
        images,
        category
    });
   
    newItem.save()
    .then(() => res.json('Item added!'))
    .catch(err => res.status(400).json('Error: ' + err));
   
   });
   

// Obtener un artículo 
router.route('/:id').get((req,res) => {
    Item.findById(req.params.id)
    .then(item => res.json(item))
    .catch(err => res.status(400).json('Error: '+err));
});

   // Eliminar articulo

router.route('/:id').delete((req,res) => {
    Item.findByIdAndDelete(req.params.id)
    .then(item => res.json('Item Eliminado'))
    .catch(err => res.status(400).json('Error: '+err));
});

// Actualizar articulo
router.route('/update/:id').post((req,res) => {
    Item.findById(req.params.id)
    .then( item => {
        item.title = req.body.title;
        item.description = req.body.description;
        item.quantity =  req.body.quantity;
        item.amount = req.body.amount;
        item.images = req.body.images;
        item.category = req.body.category;
        item.save()
        .then(() => res.json('Articulo Acutalizado'))
    .catch(err => res.status(400).json('Error: '+err));

    })
    .catch(err => res.status(400).json('Error: '+err));
});

   module.exports = router;