const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');

require('dotenv').config();

// Creando servidor express
const app = express();
const port = process.env.PORT || 5000;

// Cors Middleware, agregando parseo Json  a nuestro servidor express
app.use(cors());
app.use(bodyParser.json());


// Asignación de valores de conexión desde archivo de configuración
const uri = process.env.ATLAS_URI;
mongoose.connect(uri,{ useNewUrlParser:true, useCreateIndex:true, useUnifiedTopology:true}
    );

// Conectando a ATLAS
const connection = mongoose.connection;
// Confirmar la conexión 
connection.once('open', () => {
    console.log("Conexión con Base De Datos MongoDB establecido con éxito");
})

// Rutas

const usersRouter = require('./routes/users');
const categoriesRouter = require('./routes/categories');
const itemsRouter = require('./routes/items');

app.use('/users',usersRouter);
app.use('/categories', categoriesRouter);
app.use('/items',itemsRouter);


//Inicialización del servidor en el puerto 5000
app.listen(port, () => {
    console.log(`El servidor se ejecuta en el puerto:  ${port}`);
});